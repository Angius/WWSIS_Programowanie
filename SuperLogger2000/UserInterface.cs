﻿using System;

namespace SuperLogger2000
{
    // This class holds all messages and screens the user sees
    public class UserInterface
    {

        // This method displays the initial splash screen
        public static void DisplaySplashScreen()
        {
            Console.WriteLine(String.Join(
            Environment.NewLine,
            "                                 ",
            "           ╔════════════════════╗",
            "           ║ SUPER LOGGER 2000™ ║",
            "           ╟╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╢",
            "           ║    PRO VERSION     ║",
            "           ╚════════════════════╝",
            "              © MacroHard Inc.   ",
            "                                 "
            ));
            for (int i = 0; i < 10; i++) Console.WriteLine("");
            Console.WriteLine("                PRESS ANY KEY");

        }

        // This method asks for login credentials
        public static void DisplayLoginScreen()
        {
            Console.Clear();

            Console.WriteLine(String.Join(
            Environment.NewLine,
            "                                    ",
            "              ┏━━━━━━━━━━━━━━━━┓    ",
            "              ┃ PLEASE, LOG IN ┃    ",
            "              ┗━━━━━━━━━━━━━━━━┛    ",
            "                                    "
            ));
            for (int i = 0; i < 5; i++) Console.WriteLine("");
        }

        // This method displays an incorrect credentials message
        public static void DisplayWrongCredentialsMessage()
        {
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(String.Join(
            Environment.NewLine,
            "                                      ",
            "       ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓",
            "       ┃ INCORRECT LOGIN OR PASSWORD ┃",
            "       ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛",
            "                                      ",
            "       ┏ [1] Try again                ",
            "       ┗ [2] Exit                     "
            ));
            Console.ForegroundColor = ConsoleColor.White;
        }

        // This method displays the main menu header
        public static void DisplayMainMenuHeader(string userName)
        {
            int UserNameLength = userName.Length;
            userName = userName.ToUpper();
            int Spacer = (45 - (UserNameLength + 10)) / 2;

            Console.Clear();

            Console.WriteLine("");

            // Print top of the box
            for (int i = 0; i < Spacer; i++)
            {
                Console.Write(" ");
            }
            Console.Write("┏");
            for (int j = 0; j < UserNameLength + 9; j++)
            {
                Console.Write("━");
            }
            Console.Write("┓");

            // Print middle of the box
            Console.WriteLine("");
            for (int i = 0; i < Spacer; i++)
            {
                Console.Write(" ");
            }
            Console.Write("┃ ");
            Console.Write("HELLO, ");
            Console.Write(userName);
            Console.Write(" ┃");

            // Print bottom of the box
            Console.WriteLine("");
            for (int i = 0; i < Spacer; i++)
            {
                Console.Write(" ");
            }
            Console.Write("┗");
            for (int j = 0; j < UserNameLength + 9; j++)
            {
                Console.Write("━");
            }
            Console.Write("┛");
        }

        // This method displays last login time
        public static void DisplayLastLogin (string file, string login)
        {
            string[] lastLoginArray = Logic.GetLastLoginTime(file, login);
            string lastLoginString = lastLoginArray[1];
            Console.WriteLine("");
            Console.WriteLine("            ┏━━━━━━━━━━━━━━━━━━━━━┓    ");
            Console.WriteLine("            ┃  You visited last:  ┃");
            Console.WriteLine("            ┃ " + lastLoginString + " ┃ ");
            Console.WriteLine("            ┗━━━━━━━━━━━━━━━━━━━━━┛    ");
        }

        // This method displays worker's menu
        public static void DisplayWorkerMenu()
        {
            Console.WriteLine(String.Join(
            Environment.NewLine,
            "                                    ",
            "              ┏━━━━━━━━━━━━━━━━┓    ",
            "              ┃ [1] START WORK ┃    ",
            "              ┃ [2] LOG OUT    ┃    ",
            "              ┗━━━━━━━━━━━━━━━━┛    ",
            "                                    "
            ));
        }

        // This method displays admin menu
        public static void DisplayAdminrMenu()
        {
            Console.WriteLine(String.Join(
            Environment.NewLine,
            "                                         ",
            "        ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━┓    ",
            "        ┃ [1] CREATE USER           ┃    ",
            "        ┃ [2] CHANGE USER PASSWORD  ┃    ",
            "        ┃ [3] LOG OUT               ┃    ",
            "        ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━┛    ",
            "                                         "
            ));
        }

        // This method displays password change menu
        public static void AdminChangePassword()
        {
            Console.Clear();

            Console.WriteLine(String.Join(
            Environment.NewLine,
            "                                      ",
            "          ┏━━━━━━━━━━━━━━━━━━━━━━━━━━┓",
            "          ┃ CHANGE PASSWORD FOR USER ┃",
            "          ┗━━━━━━━━━━━━━━━━━━━━━━━━━━┛",
            "                                      "
            ));
        }

        // This method displays new user creation menu
        public static void AdminAddUser()
        {
            Console.Clear();

            Console.WriteLine(String.Join(
            Environment.NewLine,
            "                          ",
            "               ┏━━━━━━━━━━━━━━┓",
            "               ┃ ADD NEW USER ┃",
            "               ┗━━━━━━━━━━━━━━┛",
            "                          "
            ));
        }

    }
}
