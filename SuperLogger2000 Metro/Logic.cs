﻿using System;
using System.Linq;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.Windows.Threading;

using static SuperLogger2000_Metro.DataStructures;


namespace SuperLogger2000_Metro
{

    // This class holds all the logic the program uses
    public class Logic
    {
        // Enum for holding user types
        public enum UserType
        {
            admin,
            user,
            invalid
        }

        // Compare given login and passwod with ones stored in the file and returns a boolean
        public static bool UserLogin(string file, string login, string password)
        {
            string[] wholeFile = File.ReadAllLines(file);

            foreach (string line in wholeFile)
            {
                string[] lineFromFile = line.Split(';');

                if (lineFromFile[2] == login)
                {
                    if (lineFromFile[3] == password)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        // This method takes login credentials and returns the user type
        // 1 – admin
        // 2 – user
        // 0 – invalid login and/or password
        public static UserType Login(string file, string login, string pass)
        {
            if (login == "admin" && pass == "admin")
            {
                return UserType.admin;
            }
            else
            {
                if (UserLogin(file, login, pass))
                {
                    return UserType.user;
                }
                else
                {
                    return UserType.invalid;
                }
            }
        }

        // Parse date and time into a string
        public static string LogTime()
        {
            var culture = new CultureInfo("pl-PL");
            DateTime StartTime = new DateTime();
            StartTime = DateTime.Now;
            string strStartTime = StartTime.ToString(culture);

            return strStartTime;
        }


        // Return all user data based on user login
        public static string[] GetUserData(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);
            string[] notFound = new string[5] { "null_1", "null_2", "null_3", "null_4", "null_5" };

            foreach (string line in wholeFile)
            {
                string[] lineFromFile = line.Split(';');
                if (lineFromFile.Length < 3) continue;

                if (lineFromFile[2] == login)
                {
                    return lineFromFile;
                }
            }
            return notFound;
        }

        // Return most recent login time based on user login
        public static string[] GetLastLoginTime(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);
            string[] nullString = { "0", "00.00.0000 00:00:00", "11.11.1111 11:11:11" };

            for (int i = wholeFile.Length - 1; i > 0; i--)
            {
                string[] lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[2] == login)
                {
                    return lineFromFile;
                }
            }
            return nullString;
        }

        public static string PasswordGenerator(int length)
        {
            // Generate base string
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var baseString = new char[length];
            bool isPassSafeVar = false;
            var random = new Random();

            for (int i = 0; i < baseString.Length; i++)
            {
                baseString[i] = chars[random.Next(chars.Length)];
            }

            while (!isPassSafeVar)
            {
                //Add an uppercase letter
                var capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                var random2 = new Random();
                var random3 = new Random();

                baseString[random2.Next(baseString.Length)] = capitals[random3.Next(capitals.Length)];

                //Add a lowercase letter
                var lowercase = "abcdefghijklmnopqrstuvwxyz";
                var random4 = new Random();
                var random5 = new Random();

                baseString[random4.Next(baseString.Length)] = lowercase[random5.Next(lowercase.Length)];

                //Add a number
                var numbers = "0123456789";
                var random6 = new Random();
                var random7 = new Random();

                baseString[random4.Next(baseString.Length)] = numbers[random5.Next(numbers.Length)];

                if (isPassSafe(new string(baseString)))
                {
                    isPassSafeVar = true;
                }
            }
            //Final assembly
            var finalString = new String(baseString);
            return finalString;
        }

        // Take user login and generate a new password
        public static void ChangePassword(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);
            Random rnd = new Random();
            int passLength = rnd.Next(8, 15);
            var newPassword = PasswordGenerator(passLength);

            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[2] == login)
                {
                    var newUserData = string.Join(";", lineFromFile.Take(3)) + ";" + newPassword + ";" + lineFromFile[4] + ";PassChanged";
                    wholeFile[i] = newUserData;

                    File.WriteAllLines(file, wholeFile);
                    break;
                }
            }
        }

        // Take user name and surname and generate new User data
        public static string CreateUser(string file, string name, string surname)
        {
            string login = name.First() + surname;

            Random rnd = new Random();
            int passLength = rnd.Next(8, 15);
            var newPassword = PasswordGenerator(passLength); //Membership.GeneratePassword(passLength, 0);

            int i = 0;
            string finalLogin = login;

            while (GetUserData(file, finalLogin)[2] == finalLogin)
            {
                finalLogin = string.Format("{0}{1}", login, ++i);
            }

                string newUserData = name + ";" + surname + ";" + finalLogin + ";" + newPassword + ";" + LogTime() + ";New";
                File.AppendAllText(file, newUserData + Environment.NewLine);

                return finalLogin;
        }

        // Lock user
        public static void LockUser(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);

            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[2] == login)
                {
                    var newUserData = string.Join(";", lineFromFile.Take(5)) + ";Locked";
                    wholeFile[i] = newUserData;

                    File.WriteAllLines(file, wholeFile);
                    break;
                }
            }
        }

        // Unlock user
        public static void UnlockUser(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);

            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[2] == login)
                {
                    var newUserData = string.Join(";", lineFromFile.Take(5)) + ";Unlocked";
                    wholeFile[i] = newUserData;

                    File.WriteAllLines(file, wholeFile);
                    break;
                }
            }
        }

        // Get last login
        public static string GetLastLoginDate (string file, string login)
        {
            List<string> thisMonthLogins = GetThisMonthLoginTimes(file, login);
            string[] lastLoginArr = new string[2];

            if (thisMonthLogins == null)
            {
                lastLoginArr[0] = LogTime();
                lastLoginArr[1] = LogTime();
            }
            else if (thisMonthLogins.Count - 2 < 0)
            {
                lastLoginArr[0] = LogTime();
                lastLoginArr[1] = LogTime();
            }
            else
            {
                lastLoginArr = thisMonthLogins[thisMonthLogins.Count - 2].Split(';');
            }

            return lastLoginArr[0];
        }

        // Get all user's login times
        public static List<string> GetThisMonthLoginTimes(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);
            List<string> allLoginTimes = new List<string>();

            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[0] == login)
                {
                    allLoginTimes.Add(lineFromFile[1] + ";" + lineFromFile[2]);
                }
            }
            return allLoginTimes;
        }

        // Change last login time
        public static void ChangeLastLogin(string userdb, string timedb, string login)
        {
            string[] wholeFile = File.ReadAllLines(userdb);
            string[] getLine = new string[2];

            if (GetThisMonthLoginTimes(timedb, login) == null)
            {
                getLine[0] = LogTime();
                getLine[1] = LogTime();
            }
            else if (GetThisMonthLoginTimes(timedb, login).Count - 2 < 0)
            {
                getLine[0] = LogTime();
                getLine[1] = LogTime();
            }
            else
            {
                getLine = GetThisMonthLoginTimes(timedb, login)[GetThisMonthLoginTimes(timedb, login).Count - 1].Split(';');
            }


            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[2] == login)
                {
                    string newUserData = lineFromFile[0] + ";" + lineFromFile[1] + ";" + lineFromFile[2] + ";" + lineFromFile[3] +";" + getLine[0] + ";" + lineFromFile[5];
                    wholeFile[i] = newUserData;

                    File.WriteAllLines(userdb, wholeFile);
                    break;
                }
            }

        }

        // Check user status
        public static Status CheckUserStatus(string file, string login)
        {
            Status userStatus;
            Enum.TryParse(GetUserData(file, login)[5], out userStatus);

            return userStatus;
        }

        // Saves user-typed password
        public static void ChangePassword(string file, string login, string newPass)
        {
            if (isPassSafe(newPass) == true)
            {
                string[] wholeFile = File.ReadAllLines(file);
                for (int i = 0; i < wholeFile.Length; i++)
                {
                    var lineFromFile = wholeFile[i].Split(';');

                    if (lineFromFile[2] == login)
                    {
                        var newUserData = string.Join(";", lineFromFile.Take(3)) + ";" + newPass + ";" + lineFromFile[4] + ";Unlocked";
                        wholeFile[i] = newUserData;

                        File.WriteAllLines(file, wholeFile);
                        break;
                    }
                }
            }

        }

        // Checks safety of password
        public static bool isPassSafe(string password)
        {
            bool hasCapitalLetter = false;
            bool hasSmallLetter = false;
            bool hasNumber = false;

            if (password.Length < 8) return false;

            for (int i = 0; i < password.Length; i++)
            {
                if (password[i] > 64 && password[i] < 91) hasCapitalLetter = true;
                if (password[i] > 96 && password[i] < 123) hasSmallLetter = true;
                if (password[i] > 47 && password[i] < 58) hasNumber = true;
            }
            if (hasCapitalLetter == true && hasSmallLetter == true && hasNumber == true) return true;
            else return false;
        }

        //Generates login/logout data
        public static List<string> GenerateActivityLog(string file, string login)
        {
            List<string> activityTimes = GetThisMonthLoginTimes(file, login);
            List<string> activityLog = new List<string>();

            foreach (string row in activityTimes)
            {
                string[] tempRowArray = row.Split(';');

                //if (DateTime.Parse(tempRowArray[0]).Date == DateTime.Now.Date)
                //{
                    if (tempRowArray[1] != "")
                    {
                        //TimeSpan start = DateTime.Parse(tempRowArray[0]).TimeOfDay;
                        //TimeSpan end = DateTime.Parse(tempRowArray[1]).TimeOfDay;

                        activityLog.Add(tempRowArray[0] + ";" + tempRowArray[1]);
                    }
                    else
                    {
                        //TimeSpan start = DateTime.Parse(tempRowArray[0]).TimeOfDay;
                        activityLog.Add(tempRowArray[0] + ";");
                    }
                //}
            }
            return activityLog;
        }

        // Calculates time worked
        public static string CalculateTimeWorked(string file, string login, DateTime date)
        {
            TimeSpan timeWorked = TimeSpan.FromMilliseconds(0);

            string[] wholeFile = File.ReadAllLines(file);
            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[0] == login)
                {
                    if (DateTime.Parse(lineFromFile[1]).Date == date.Date)
                    {
                        if (lineFromFile[2] != "")
                        {
                            TimeSpan timeWorkedTmp = DateTime.Parse(lineFromFile[2]) - DateTime.Parse(lineFromFile[1]);
                            timeWorked += timeWorkedTmp;
                        }
                    }
                }
            }

            return timeWorked.Hours.ToString() + "h " + timeWorked.Minutes.ToString() + "m";
        }

        // Creates a monthly report
        public static List<string> GenerateMonthlyReport(string file, string login)
        {
            List<string> report = new List<string>();

            string[] wholeFile = File.ReadAllLines(file);
            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[0] == login)
                {
                    if (DateTime.Parse(lineFromFile[1]).Month == DateTime.Now.Month)
                    {
                        string tmpDate = DateTime.Parse(lineFromFile[1]).ToString("d.MM", CultureInfo.InvariantCulture);
                        string tmpTime = CalculateTimeWorked(file, login, DateTime.Parse(lineFromFile[1]));

                        report.Add(tmpDate + ";" + tmpTime);
                    }
                }
            }

            List<string> uniqueReport = report.Distinct().ToList();

            return uniqueReport;
        }

        // Get time from file
        public static TimeSpan GetTimeWorkedToday(string file, string login)
        {
            TimeSpan timeWorked = TimeSpan.FromSeconds(0);

            string[] wholeFile = File.ReadAllLines(file);
            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[0] == login)
                {
                    if (DateTime.Parse(lineFromFile[1]).Date == DateTime.Now.Date)
                    {
                        if (lineFromFile[2] != "")
                        {
                            TimeSpan timeWorkedTmp = DateTime.Parse(lineFromFile[2]) - DateTime.Parse(lineFromFile[1]);
                            timeWorked += timeWorkedTmp;
                        }
                    }
                }
            }

            return timeWorked;
        }


        //Check if user exists
        public static bool DoesUserExist(string file, string login)
        {
            List<string> logins = new List<string>();
            string[] wholeFile = File.ReadAllLines(file);

            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');
                logins.Add(lineFromFile[2]);
            }


            if (logins.Contains(login))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}