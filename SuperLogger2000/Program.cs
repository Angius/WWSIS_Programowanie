﻿using System;
using System.Text;
using System.IO;

namespace SuperLogger2000
{
    // Main class of the Super Logger 2000
    class Program
    {
        static void Main(string[] args)
        {
            // Resize window and set colour
            Console.SetWindowSize(45, 20);
            Console.SetBufferSize(45, 20);
            Console.OutputEncoding = Encoding.UTF8;
            Console.ForegroundColor = ConsoleColor.White;

            // Set file variables
            string userdb = "userdb.txt";
            string timedb = "timedb.txt";

            // Set variables
            int userType = 0;
            string userTypeText = "";
            string login = "";
            string pass = "";

            // Check if files exist, if not create them
            if (!File.Exists(userdb))
            {
                File.Create("userdb.txt");
            }
            if (!File.Exists(timedb))
            {
                File.Create("timedb.txt");
            }
            // Display splash screen
            UserInterface.DisplaySplashScreen();
            Console.ReadKey();

            // Display login screen and take login credentials
            while (true)
            {

                UserInterface.DisplayLoginScreen();
                Console.Write(" ┏ login:    ");
                login = Console.ReadLine();
                Console.Write(" ┗ password: ");
                pass = Logic.Password();

                // Set user type in both int and 
                userType = Logic.Login(userdb, login, pass);
                Console.WriteLine(userType);

                switch (userType)
                {
                    case 1:
                        userTypeText = "Admin";
                        break;
                    case 2:
                        string[] userData = Logic.GetUserData(userdb, login);
                        userTypeText = userData[0];
                        break;
                    default:
                        userTypeText = "ERROR";
                        break;
                }

                if (userType == 1)
                {
                   while (true)
                   {
                        UserInterface.DisplayMainMenuHeader(userTypeText);
                        Menus.AdminMenu(userdb);
                   }
                }
                else if (userType == 2)
                {
                    UserInterface.DisplayMainMenuHeader(userTypeText);
                    UserInterface.DisplayLastLogin(timedb, login);
                    Menus.WorkerMenu(timedb, login);
                }
                else
                {
                    Menus.WrongCredentialsMenu();
                }
            }
        }
    }
}