﻿using System;
using System.IO;

namespace SuperLogger2000
{
    // This class holds all the menus for users
    class Menus
    {

        // This method displays the menu you get when password or login are incorrect
        public static void WrongCredentialsMenu()
        {
            UserInterface.DisplayWrongCredentialsMessage();
            while (true)
            {
                char key = Console.ReadKey(true).KeyChar;
                if (key == '1')
                {
                    return;// go back to login screen
                }
                else if (key == '2')
                {
                    Environment.Exit(0); // close the program completely
                }
                else
                {
                    // keep listening for valid input
                }
            }
        }

        // The menu admin sees
        public static void AdminMenu(string file)
        {
            UserInterface.DisplayAdminrMenu();
            while (true)
            {
                char key = Console.ReadKey(true).KeyChar;
                if (key == '1') // Create user
                {
                    UserInterface.AdminAddUser();

                    Console.Write(" ┏ name:    ");
                    string name = Console.ReadLine();
                    Console.Write(" ┗ surname: ");
                    string surname = Console.ReadLine();

                    string login = Logic.CreateUser(file, name, surname);
                    
                    string password = Logic.GetUserData(file, login)[3];
                    string date = Logic.GetUserData(file, login)[4];

                    Console.Write(" ┏ name:        ");
                    Console.WriteLine(name);
                    Console.Write(" ┣ surname:     ");
                    Console.WriteLine(surname);
                    Console.Write(" ┣ login:       ");
                    Console.WriteLine(login);
                    Console.Write(" ┣ password:    ");
                    Console.WriteLine(password);
                    Console.Write(" ┗ date joined: ");
                    Console.WriteLine(date);
                    

                    Console.ReadKey(true);
                    return;

                }
                else if (key == '2') // Change password
                {
                    UserInterface.AdminChangePassword();
                    Console.Write(" ┏ login:        ");
                    string loginToChange = Console.ReadLine();

                    Logic.ChangePassword(file, loginToChange);

                    Console.Write(" ┗ new password: ");
                    Console.Write(Logic.GetUserData(file, loginToChange)[3]);

                    Console.ReadKey(true);
                    return;
                }
                else if (key == '3')
                {
                    break;
                }
                else
                {
                    // keep listening for valid input
                }
            }
        }

        // The menu worker sees
        public static void WorkerMenu(string file, string login)
        {

            UserInterface.DisplayWorkerMenu();

            bool WorkBegun = false;

            while (true)
            {
                char key = Console.ReadKey(true).KeyChar;
                if (key == '1')
                {
                    if (!WorkBegun)
                    {
                        string time = login + ";" + Logic.LogTime();
                        File.AppendAllText(file, time);
                        Console.WriteLine("Start time logged: " + Logic.LogTime());
                        WorkBegun = true;
                    }
                    else
                    {
                        Console.WriteLine("Time is already being logged!");
                    }
                }
                else if (key == '2')
                {
                    if (WorkBegun)
                    {
                        string time = ";" + Logic.LogTime() + Environment.NewLine;
                        File.AppendAllText(file, time);
                        Console.WriteLine("Start time logged: " + Logic.LogTime());
                        return;
                    }
                    else
                    {
                        Console.WriteLine("You need to begin the work first");
                    }
                }
                else
                {
                    // keep listening for valid input
                }
            }
        }
    }
}
