﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SuperLogger2000_Metro
{
    public class DataStructures
    {
        // User status enum
        public enum Status
        {
            New,
            Locked,
            Unlocked,
            PassChanged
        }

        // User structure
        public class User
        {
            public string Name { get; set; }

            public string Surname { get; set; }

            public string Login { get; set; }

            public string Password { get; set; }

            public string LastLogin { get; set; }

            public Status Status { get; set; }
        }

        public static class UserService
        {
            public static List<User> ReadFile(string filepath)
            {
                var lines = File.ReadAllLines(filepath);

                var data = from l in lines.Skip(1)
                           let split = l.Split(';')
                           where split.Length == 6
                           select new User
                           {
                               Name = split[0],
                               Surname = split[1],
                               Login = split[2],
                               Password = split[3],
                               LastLogin = split[4],
                               Status = (Status)Enum.Parse(typeof(Status), split[5])
                           };

                return data.ToList();
            }
        }

        // Activity structure
        public class Activity
        {
            public string Start { get; set; }

            public string End { get; set; }
        }

        public static class ActivityService
        {
            public static List<Activity> ReadFile(string file, string login)
            {
                var lines = Logic.GenerateActivityLog(file, login);

                var data = from l in lines
                           let split = l.Split(';')
                           where split.Length == 2
                           select new Activity
                           {
                               Start = split[0],
                               End = split[1]
                           };

                return data.ToList();
            }
        }

        public class MonthlyReport
        {
            public string Date { get; set; }

            public string Time { get; set; }
        }

        public static class MonthlyReportService
        {
            public static List<MonthlyReport> ReadFile(string file, string login)
            {
                var lines = Logic.GenerateMonthlyReport(file, login); // generates a list with possible doubles

                var data = from l in lines
                           let split = l.Split(';')
                           where split.Length == 2
                           select new MonthlyReport
                           {
                               Date = split[0],
                               Time = split[1]
                           };

                return data.Distinct().ToList();
            }
        }


    }

}
