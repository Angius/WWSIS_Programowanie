﻿using System;
using System.Linq;
using System.IO;
using System.Globalization;
using System.Web.Security;

namespace SuperLogger2000
{

    // This class holds all the logic the program uses
    public class Logic
    {
        // This method takes login credentials and returns the user type
        // 1 – admin
        // 2 – user
        // 0 – invalid login and/or password
        public static int Login(string file, string login, string pass)
        {
            if (login == "admin" && pass == "admin")
            {
                return 1;
            }
            else
            {
                if (UserLogin(file, login, pass))
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
        }

        // This method obstructs the password with asterisks and returns an uppercase string
        public static string Password()
        {
            string Pass = "";
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);

                // Ignore any key out of range
                if (((int)key.Key) >= 65 && ((int)key.Key <= 90))
                {
                    // Append the character to the password
                    Pass = Pass.Insert(Pass.Length, key.KeyChar.ToString());
                    Console.Write("*");
                }
                // Exit if Enter key is pressed
            } while (key.Key != ConsoleKey.Enter);
            Console.WriteLine();
            return Pass;
        }


        // Parse date and time into a string
        public static string LogTime()
        {
            var culture = new CultureInfo("pl-PL");
            DateTime StartTime = new DateTime();
            StartTime = DateTime.Now;
            string strStartTime = StartTime.ToString(culture);

            return strStartTime;
        }

        // Compare given login and passwod with ones stored in the file and returns a boolean
        public static bool UserLogin(string file, string login, string password)
        {
            string[] wholeFile = File.ReadAllLines(file);

            foreach (string line in wholeFile)
            {
                string[] lineFromFile = line.Split(';');

                if (lineFromFile[2] == login)
                {
                    if (lineFromFile[3] == password)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        // Return all user data based on user login
        public static string[] GetUserData(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);
            string[] notFound = new string[5] { "null_1", "null_2", "null_3", "null_4", "null_5" };

            foreach (string line in wholeFile)
            {
                string[] lineFromFile = line.Split(';');
                if (lineFromFile.Length < 3) continue;

                if (lineFromFile[2] == login)
                {
                    return lineFromFile;
                }
            }
            return notFound;
        }

        // Return most recent login time based on user login
        public static string[] GetLastLoginTime(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);
            string[] nullString = { "0", "00.00.0000 00:00:00", "11.11.1111 11:11:11" };

            for (int i = wholeFile.Length - 1; i > 0; i--)
            {
                string[] lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[2] == login)
                {
                    return lineFromFile;
                }
            }
            return nullString;
        }

        // Take user login and generate a new password
        public static string ChangePassword(string file, string login)
        {
            string[] wholeFile = File.ReadAllLines(file);
            Random rnd = new Random();
            int passLength = rnd.Next(8, 15);
            var newPassword = PasswordGenerator(passLength);

            for (int i = 0; i < wholeFile.Length; i++)
            {
                var lineFromFile = wholeFile[i].Split(';');

                if (lineFromFile[2] == login)
                {
                    var newUserData = string.Join(";", lineFromFile.Take(3)) + ";" + newPassword + ";" + lineFromFile[4];
                    wholeFile[i] = newUserData;

                    File.WriteAllLines(file, wholeFile);
                    break;
                }
            }
            return "Null";
        }

        public static string PasswordGenerator(int length)
        {
            // Generate base string
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var baseString = new char[length];
            var random = new Random();

            for (int i = 0; i < baseString.Length; i++)
            {
                baseString[i] = chars[random.Next(chars.Length)];
            }

            //Add an uppercase letter
            var capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var random2 = new Random();
            var random3 = new Random();

            baseString[random2.Next(baseString.Length)] = capitals[random3.Next(capitals.Length)];

            //Add a lowercase letter
            var lowercase = "abcdefghijklmnopqrstuvwxyz";
            var random4 = new Random();
            var random5 = new Random();

            baseString[random4.Next(baseString.Length)] = lowercase[random5.Next(lowercase.Length)];

            //Add a number
            var numbers = "0123456789";
            var random6 = new Random();
            var random7 = new Random();

            baseString[random4.Next(baseString.Length)] = numbers[random5.Next(numbers.Length)];

            //Final assembly
            var finalString = new String(baseString);

            return finalString;
        }

        // Take user name and surname and generate new User data
        public static string CreateUser(string file, string name, string surname)
        {
            string login = name.First() + surname;

            Random rnd = new Random();
            int passLength = rnd.Next(8, 15);
            var newPassword = PasswordGenerator(passLength); //Membership.GeneratePassword(passLength, 0);

            int i = 0;
            string finalLogin = login;

            while (GetUserData(file, finalLogin)[2] == finalLogin)
            {
                finalLogin = string.Format("{0}{1}", login, ++i);
            }

                string newUserData = name + ";" + surname + ";" + finalLogin + ";" + newPassword + ";" + LogTime();
                File.AppendAllText(file, Environment.NewLine + newUserData);

                return finalLogin;
        }
    }
}