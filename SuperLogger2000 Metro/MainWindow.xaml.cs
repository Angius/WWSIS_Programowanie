﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MahApps.Metro;
using MahApps.Metro.Controls;

using static SuperLogger2000_Metro.Logic;
using static SuperLogger2000_Metro.DataStructures;

namespace SuperLogger2000_Metro
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        // Variable definitions
        private string login;
        private string pass;

        private int badLoginCount = 0;

        UserType UserType = new UserType();
        Status userStatus = new Status();

        private string userdb = "userdb.txt";
        private string timedb = "timedb.txt";

        public MainWindow()
        {
            InitializeComponent();

            // Check if files exist, if not create them
            if (!File.Exists(userdb))
            {
                File.Create("userdb.txt");
            }
            if (!File.Exists(timedb))
            {
                File.Create("timedb.txt");
            }

            // Create data binding
            DataContext = new
            {
                users = UserService.ReadFile(userdb),
                activitylog = ActivityService.ReadFile(timedb, login),
                monthlyreport = MonthlyReportService.ReadFile(timedb, login)
            };
        }


        // Hover effects
        private void Login_MouseEnter(object sender, MouseEventArgs e)
        {
            LoginText.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
        }

        private void Pass_MouseEnter(object sender, MouseEventArgs e)
        {
            PassText.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
        }

        private void Login_MouseLeave(object sender, MouseEventArgs e)
        {
            LoginText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 204, 204));
        }

        private void Pass_MouseLeave(object sender, MouseEventArgs e)
        {
            PassText.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 204, 204));
        }

        private void LoginSend_MouseEnter(object sender, MouseEventArgs e)
        {
            LoginSend.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
        }

        private void LoginSend_MouseLeave(object sender, MouseEventArgs e)
        {
            LoginSend.Foreground = new SolidColorBrush(Color.FromArgb(255, 204, 204, 204));
        }


        // Check login
        private void LoginSend_Click(object sender, RoutedEventArgs e)
        {
            login = Login.Text;
            pass = Pass.Password;

            UserType = Login(userdb, login, pass);

            if (UserType == UserType.admin)
            {
                badLoginCount = 0;
                BadLoginInfo.Text = "";

                DataContext = null;
                DataContext = new
                {
                    users = UserService.ReadFile(userdb),
                    activitylog = ActivityService.ReadFile(timedb, login),
                    monthlyreport = MonthlyReportService.ReadFile(timedb, login)
                };

                Login.Text = "";
                Pass.Password = "";

                AdminMenuFlyout.IsOpen = true;
            }
            else if (UserType == UserType.user)
            {
                badLoginCount = 0;
                BadLoginInfo.Text = "";

                userStatus = CheckUserStatus(userdb, login);

                IsCloseButtonEnabled = false;

                string time = login + ";" + LogTime() + ";";
                File.AppendAllText(timedb, time);

                string[] lastLogin = GetLastLoginTime(userdb, login);
                string lastLoginStr = GetLastLoginDate(timedb, login);

                UserWelcome.Text = "Hello " + login + "!";
                UserLastLogin.Text = "Last login: " + lastLoginStr;

                DataContext = null;
                DataContext = new
                {
                    users = UserService.ReadFile(userdb),
                    activitylog = ActivityService.ReadFile(timedb, login),
                    monthlyreport = MonthlyReportService.ReadFile(timedb, login)
                };

                Login.Text = "";
                Pass.Password = "";

                UserMenuFlyout.IsOpen = true;

                if (userStatus == Status.Locked)
                {
                    Popup_LockedByAdmin.IsOpen = true;
                }
                else if (userStatus == Status.New)
                {
                    Popup_NewAccountSetPassword.IsOpen = true;
                    ChangePasswordHeader.Text = "Please, create your own password.";
                    TimerWork();
                }
                else if (userStatus == Status.PassChanged)
                {
                    Popup_NewAccountSetPassword.IsOpen = true;
                    ChangePasswordHeader.Text = "Please, create a new password.";
                    TimerWork();
                }
                else
                {
                    TimerWork();
                }

            }
            else if (UserType == UserType.invalid)
            {
                if (DoesUserExist(userdb, login))
                {
                    if (badLoginCount >= 3)
                    {
                        BadLoginInfo.Text = "Account has been locked. Contact the administrator.";
                        BadLoginInfo.Foreground = Brushes.Red;

                        LockUser(userdb, login);

                        Login.Text = "";
                        Pass.Password = "";
                    }
                    else
                    {
                        badLoginCount++;

                        int triesLeftInt = 3 - badLoginCount;
                        string triesLeftStr = triesLeftInt.ToString();

                        BadLoginInfo.Text = "Incorrect credentials! Tries left: " + triesLeftStr;

                        Login.Text = "";
                        Pass.Password = "";
                    }
                }
                else
                {
                    BadLoginInfo.Text = "User does not exist";
                    Login.Text = "";
                    Pass.Password = "";
                }
            }
        }

        // Log out admin
        private void LogoutAdmin_Click(object sender, RoutedEventArgs e)
        {
            AdminMenuFlyout.IsOpen = false;

            badLoginCount = 0;
            BadLoginInfo.Text = "";
        }

        // Admin menu commands
        private void Button_CreateUser_Click(object sender, RoutedEventArgs e)
        {
            popupCreateUser.IsOpen = true;
        }

        private void Button_ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            popupChangeUserPassword.IsOpen = true;
        }

        private void Button_LockUser_Click(object sender, RoutedEventArgs e)
        {
            popupLockUser.IsOpen = true;
        }

        private void Button_UnlockUser_Click(object sender, RoutedEventArgs e)
        {
            popupUnlockUser.IsOpen = true;
        }


        // Add new user
        private void Submit_CreateUser_Click(object sender, RoutedEventArgs e)
        {
            CreateUser(userdb, Name_CreateUser.Text, Surname_CreateUser.Text);

            DataContext = null;
            DataContext = new
            {
                users = UserService.ReadFile(userdb),
                activitylog = ActivityService.ReadFile(timedb, login),
                monthlyreport = MonthlyReportService.ReadFile(timedb, login)
            };

            popupCreateUser.IsOpen = false;

            Name_CreateUser.Text = "";
            Surname_CreateUser.Text = "";
        }

        // Change user password
        private void Submit_ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword(userdb, Login_ChangePassword.Text);

            DataContext = null;
            DataContext = new
            {
                users = UserService.ReadFile(userdb),
                activitylog = ActivityService.ReadFile(timedb, login),
                monthlyreport = MonthlyReportService.ReadFile(timedb, login)
            };

            popupChangeUserPassword.IsOpen = false;

            Login_ChangePassword.Text = "";

            badLoginCount = 0;
            BadLoginInfo.Text = "";
        }

        // Lock user
        private void Submit_LockUser_Click(object sender, RoutedEventArgs e)
        {
            LockUser(userdb, Login_LockUser.Text);

            DataContext = null;
            DataContext = new
            {
                users = UserService.ReadFile(userdb),
                activitylog = ActivityService.ReadFile(timedb, login),
                monthlyreport = MonthlyReportService.ReadFile(timedb, login)
            };

            popupLockUser.IsOpen = false;

            Login_LockUser.Text = "";

            badLoginCount = 0;
            BadLoginInfo.Text = "";
        }

        // Unlock user
        private void Submit_UnlockUser_Click(object sender, RoutedEventArgs e)
        {
            UnlockUser(userdb, Login_UnlockUser.Text);

            DataContext = null;
            DataContext = new
            {
                users = UserService.ReadFile(userdb),
                activitylog = ActivityService.ReadFile(timedb, login),
                monthlyreport = MonthlyReportService.ReadFile(timedb, login)
            };

            popupUnlockUser.IsOpen = false;

            Login_UnlockUser.Text = "";

            badLoginCount = 0;
            BadLoginInfo.Text = "";
        }

        // Logout user
        private void LogoutUser_Click(object sender, RoutedEventArgs e)
        {
            string time = LogTime() + Environment.NewLine;
            File.AppendAllText(timedb, time);

            if (_timer != null)
            {
                _timer.Stop();
            }

            ChangeLastLogin(userdb, timedb, login);

            Flyout_ActivityLog.IsOpen = false;
            flipFlop_ActivityLog = false;

            Flyout_MonthlyReport.IsOpen = false;
            flipFlop_MonthlyReport = false;

            Popup_LockedByAdmin.IsOpen = false;

            UserMenuFlyout.IsOpen = false;

            IsCloseButtonEnabled = true;

            badLoginCount = 0;
            BadLoginInfo.Text = "";
        }

        // Change password after a monit
        private void Submit_ChangeNewPassword_Click(object sender, RoutedEventArgs e)
        {
            string pass1 = ChangeNewPassword1.Password;
            string pass2 = ChangeNewPassword2.Password;

            if (pass1 == pass2)
            {
                if (isPassSafe(pass1))
                {
                    ChangePassword(userdb, login, pass1);
                    Popup_NewAccountSetPassword.IsOpen = false;

                    ChangeNewPassword1.Password = "";
                    ChangeNewPassword2.Password = "";
                }
                else
                {
                    ChangePasswordHeader.Text = "PASSWORD NOT SECURE!";
                    ChangePasswordHeader.Foreground = Brushes.Red;
                }
            }
            else
            {
                ChangePasswordHeader.Text = "PASSWORDS ARE DIFFERENT!";
                ChangePasswordHeader.Foreground = Brushes.Red;
            }
        }

        bool flipFlop_ActivityLog = false;
        private void Button_ActivityLog_Click(object sender, RoutedEventArgs e)
        {
            if (!flipFlop_ActivityLog)
            {
                Flyout_ActivityLog.IsOpen = true;
                flipFlop_ActivityLog = true;
            }
            else
            {
                Flyout_ActivityLog.IsOpen = false;
                flipFlop_ActivityLog = false;
            }
        }

        bool flipFlop_MonthlyReport = false;
        private void Button_MonthlyReport_Click(object sender, RoutedEventArgs e)
        {
            if (!flipFlop_MonthlyReport)
            {
                Flyout_MonthlyReport.IsOpen = true;
                flipFlop_MonthlyReport = true;
            }
            else
            {
                Flyout_MonthlyReport.IsOpen = false;
                flipFlop_MonthlyReport = false;
            }
        }

        // Timer
        DispatcherTimer _timer;
        TimeSpan _time;

        private void TimerWork()
        {
            _time = GetTimeWorkedToday(timedb, login);

            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                TimerDisplay.Text = _time.ToString("c");
                _time = _time.Add(TimeSpan.FromSeconds(1));
            }, Application.Current.Dispatcher);

            _timer.Start();
        }


    }
}
