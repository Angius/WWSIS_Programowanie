﻿using System;
using SuperLogger2000;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SuperLogger2000_UT
{
    [TestClass]
    public class UnitTest1
    {
        // Logic.Login test

        [TestMethod]
        public void TestLoginAdmin()
        {
            //arrange
            string file = @"..\..\..\SuperLogger2000\bin\Debug\userdb.txt";
            string login = "admin";
            string pass = "admin";

            int expected = 1;

            //act
            int actual = Logic.Login(file, login, pass);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestLoginUser()
        {
            //arrange
            string file = @"..\..\..\SuperLogger2000\bin\Debug\userdb.txt";
            string login = "Kuba";
            string pass = "Jnowak";

            int expected = 2;

            //act
            int actual = Logic.Login(file, login, pass);

            //assert
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void TestLoginIncorrect()
        {
            //arrange
            string file = @"..\..\..\SuperLogger2000\bin\Debug\userdb.txt";
            string login = "Hackerman";
            string pass = "letmein";

            int expected = 0;

            //act
            int actual = Logic.Login(file, login, pass);

            //assert
            Assert.AreEqual(expected, actual);
        }

        // Logic.UserLogin test

        [TestMethod]
        public void TestUserLoginAllCorrect()
        {
            //arrange
            string file = @"..\..\..\SuperLogger2000\bin\Debug\userdb.txt";
            string login = "Kuba";
            string pass = "Jnowak";

            bool expected = true;

            //act
            bool actual = Logic.UserLogin(file, login, pass);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestUserLoginIncorrectLogin()
        {
            //arrange
            string file = @"..\..\..\SuperLogger2000\bin\Debug\userdb.txt";
            string login = "Hackerman";
            string pass = "Jnowak";

            bool expected = false;

            //act
            bool actual = Logic.UserLogin(file, login, pass);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestUserLoginIncorrectPassword()
        {
            //arrange
            string file = @"..\..\..\SuperLogger2000\bin\Debug\userdb.txt";
            string login = "Kuba";
            string pass = "letmein";

            bool expected = false;

            //act
            bool actual = Logic.UserLogin(file, login, pass);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestUserLoginAllIncorrect()
        {
            //arrange
            string file = @"..\..\..\SuperLogger2000\bin\Debug\userdb.txt";
            string login = "Hackerman";
            string pass = "letmein";

            bool expected = false;

            //act
            bool actual = Logic.UserLogin(file, login, pass);

            //assert
            Assert.AreEqual(expected, actual);
        }

    }
}
